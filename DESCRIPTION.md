This app packages Astral Version <upstream>0e225d6ec4</upstream>.

## Organize Your GitHub Stars With Ease

Astral is the best way to manage your starred repositories on GitHub using tags, filters, notes and a powerful search feature. 

### Your GitHub Stars. Organized.

Astral pulls down all of your starred repositories from GitHub, and allows you to organize them using a simple & intuitive tagging system.

### Features

* Simple tagging system
* Rule-based filters
* Lightning-fast search
* Readmes at a glance
* Jot down notes
* Free & open source
